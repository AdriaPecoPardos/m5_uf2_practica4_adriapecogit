import java.util.Scanner;

/**
 * <h2> Clase calculadora, se utiliza para hacer calculos</h2>
 * 
 * 
 * 
 * @version 1.1.0
 * @author apeco
 * @since 24/02/2022 
 */

public class LaCalculadora_Adria {
	
	/**
	 * atributo reader de la clase Scanner
	 */
	static Scanner reader = new Scanner(System.in);
	
	
	/**
	 * metodo main 
	 * @param args clase String[] 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                        resultat = divisio(op1, op2);
                        if (resultat == -99)
                            System.out.print("No ha fet la divisi�");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opci� erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	/**
	 * metodo que muestra una advertencia conforme a que se ha cometido un error al introducir los valores
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}



    /**
     * metodo donde se hace la suma de dos valores numericos
     * @param a de la clase int
     * @param b de la clase int
     * @return res la suma de a y b 
     */
	public static int suma(int a, int b) { /* funci� */
		int res;
		res = a + b;
		return res;
	}
	
	/**
     * metodo donde se hace la restade dos valores numericos
     * @param a de la clase int
     * @param b de la clase int
     * @return res la resta de a y b 
     */
	public static int resta(int a, int b) { /* funci� */
		int res;
		res = a - b;
		return res;
	}
	
	
	/**
     * metodo donde se hace la multiplicacion de dos valores numericos
     * @param a de la clase int
     * @param b de la clase int
     * @return resultado de la multiplicacion de a y b 
     */
	public static int multiplicacio(int a, int b) { /* funci� */
		int res;
		res = a * b;
		return res;
	}
	
	
	/**
	 * metodo donde hace la division entre dos numeros, te pide por pantalla si quieres la division normal
	 * o el modulo 
	 * @param a de la clase int
	 * @param b de la clase int
	 * @return resultado de la division entre a y b
	 */
	public static int divisio(int a, int b) { /* funci� */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci� incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}

	/*public static  int divideix(int num1, int num2) {
		int res;
		if (num2 == 0)
			throw new java.lang.ArithmeticException("Divisi� entre zero");
		else 
			res = num1/num2;
			return res; //Adria Peco
	}*/


	/**
	 *	mwtodo que pide por pantalla un caracter
	 * @return un char
	 */
	public static char llegirCar() // funci�
	{
		char car;

		System.out.print("Introdueix un car�cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	
	
	/**
	 * metodo que pide por pantalla un numero para hacer los calculos
	 * @return int
	 */
	public static int llegirEnter() // funci�
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	
	
	/**
	 * metodo que muestra por pantalla el resultado de las operaciones
	 * @param res de la clase int
	 */
	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio �s " + res);
	}
	
	
	/**
	 * metodo void que muestra por pantalla las intrucciones/funcionalidades que tiene la calculadora
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci� i ");
	}

}
